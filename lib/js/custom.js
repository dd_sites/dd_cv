// Flex slider 
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
  	animation: "fade",
    controlNav: false,
	  directionNav: false,
    pauseOnHover: false,
    pauseOnAction: true,
    slideshowSpeed: 8000,
    randomize: true
  });
});

// Box animation 


$(document).ready(function() {
    $('.content-d').hide();
    $('.box-d a').click(function() {
        var x =  $(this).attr("id");
        var $item = $('.' + x);
        if (!$item.is(":visible")) $('.content-d').hide('10','swing');
		$($item).slideToggle(400,'jswing');
        return false;
    });
}); 

/* Twitter */

/* Twitter #1 */

jQuery(function($){
   $(".tweet").tweet({
      username: "deltadave",
      join_text: "auto",
      avatar_size: 0,
      count: 5,
      refresh_interval: 60,
      auto_join_text_default: "we said,",
      auto_join_text_ed: "we",
      auto_join_text_ing: "we were",
      auto_join_text_reply: "we replied to",
      auto_join_text_url: "we were checking out",
      loading_text: "loading tweets...",
      template: "{text}"
   });
}); 

/* Twitter #2 */

jQuery(function($){
   $(".ctweet").tweet({
      query: "#vfx",
      join_text: "auto",
      avatar_size: 0,
      count: 5,
      refresh_interval: 60,
      auto_join_text_default: "we said,",
      auto_join_text_ed: "we",
      auto_join_text_ing: "we were",
      auto_join_text_reply: "we replied to",
      auto_join_text_url: "we were checking out",
      loading_text: "loading tweets...",
      template: "{text}"
   });
}); 



/* Tooltip */

$('.tip').tooltip();


/* prettyPhoto */

jQuery("a[class^='prettyPhoto']").prettyPhoto({
overlay_gallery: false, social_tools: false
});

/* email obfuscation */

$('#my-email').html(function(){
	var e = "dave";
	var a = "@";
	var d = "deltadave";
	var c = ".net";
	var h = 'mailto:' + e + a + d + c;
	$(this).parent('a').attr('href', h);
	return e + a + d + c;
});

$( ".accordion" ).accordion({
collapsible: true
});

/* bounce socialmedia icons 

$('#icon-twitter').mouseenter(function() {
    $(this).effect("bounce", { times: 2 }, { distance: 20 }, "easeOutQuad", 500);
}); 
*/

// Hide all the tooltips
$("#jquery li").each(function() {
  $("a strong", this).css("opacity", "0");
});

$("#jquery li").hover(function() { // Mouse over
  $(this)
    .stop().fadeTo(500, 1)
    .siblings().stop().fadeTo(500, 0.2);

  $("a strong", this)
    .stop()
    .animate({
      opacity: 1,
      top: "-10px"
    }, 300);

}, function() { // Mouse out
  $(this)
    .stop().fadeTo(500, 1)
    .siblings().stop().fadeTo(500, 1);

  $("a strong", this)
    .stop()
    .animate({
      opacity: 0,
      top: "-1px"
    }, 300);
});

// hide project list 

$(document).ready(function() {
	 
	//ACCORDION BUTTON ACTION (ON CLICK DO THE FOLLOWING)
	$('.accordionButton').click(function() {

		//REMOVE THE ON CLASS FROM ALL BUTTONS
		$('.accordionButton').removeClass('on');
		  
		//NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
	 	$('.accordionContent').slideUp('normal');
   
		//IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
		if($(this).next().is(':hidden') == true) {
			
			//ADD THE ON CLASS TO THE BUTTON
			$(this).addClass('on');
			  
			//OPEN THE SLIDE
			$(this).next().slideDown('normal');
		 } 
		  
	 });
	  
	
	/*** REMOVE IF MOUSEOVER IS NOT REQUIRED ***/
	
	//ADDS THE .OVER CLASS FROM THE STYLESHEET ON MOUSEOVER 
	$('.accordionButton').mouseover(function() {
		$(this).addClass('over');
		
	//ON MOUSEOUT REMOVE THE OVER CLASS
	}).mouseout(function() {
		$(this).removeClass('over');										
	});
	
	/*** END REMOVE IF MOUSEOVER IS NOT REQUIRED ***/
	
	
	/********************************************************************************************************************
	CLOSES ALL S ON PAGE LOAD
	********************************************************************************************************************/	
	$('.accordionContent').hide();

});

